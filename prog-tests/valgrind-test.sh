#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
ORANGE='\033[0;33m'
BLUE='\033[0;34m'

VALGRIND_OPTIONS="--leak-check=full --show-leak-kinds=all --error-exitcode=5"
TEST_PATH='CZE/*_in.txt'

if [ -f /tmp/test/prog-test.env ]; then
  source /tmp/test/prog-test.env
fi

CONTAINER_TEST_ROOT='/tmp/test/'
PROGRAM="${CONTAINER_TEST_ROOT}a.out"
CONTAINER_TEST_PATH="${CONTAINER_TEST_ROOT}${TEST_PATH}"
# if container test path is empty or does not exist, run the program with no input
if [ -d "$CONTAINER_TEST_PATH" ]; then
    for IN_FILE in $CONTAINER_TEST_PATH; do
      VALGRIND_OUT=$(valgrind $VALGRIND_OPTIONS $PROGRAM < "$IN_FILE" 2>&1)
      if [ $? -eq 5 ]; then
        echo -e "${RED}Fail: $IN_FILE${NC}";
        echo -e "${ORANGE}Valgrind output:${NC}\n\n";
        echo -e "$VALGRIND_OUT\n\n";
        exit 1;
      else
        echo -e "${GREEN}VALGRIND OK: $IN_FILE${NC}";
      fi
    done
  else
    echo -e "${BLUE} ------ Running the program with no input ------ ${NC}\n";
    VALGRIND_OUT=$(valgrind $VALGRIND_OPTIONS $PROGRAM  2>&1)
    if [ $? -eq 5 ]; then
      echo -e "${RED}Valgrind: Fail${NC}";
      echo -e "${ORANGE}Valgrind output:${NC}\n\n";
      echo -e "$VALGRIND_OUT\n\n";
      exit 1;
    else
      echo -e "${GREEN}VALGRIND: OK${NC}";
    fi
    exit 0;
fi
