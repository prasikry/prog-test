#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
ORANGE='\033[0;33m'
BLUE='\033[0;34m'

TEST_PATH='CZE/*_in.txt'

if [ -f /tmp/test/prog-test.env ]; then
  source /tmp/test/prog-test.env
fi

CONTAINER_TEST_ROOT='/tmp/test/'
PROGRAM="${CONTAINER_TEST_ROOT}a.out"
CONTAINER_TEST_PATH="${CONTAINER_TEST_ROOT}${TEST_PATH}"
# if test path is empty or does not exist, run the program with no input
if [ -f "$CONTAINER_TEST_PATH" ]; then
    for IN_FILE in $CONTAINER_TEST_PATH; do
      REF_FILE=$(echo -n "$IN_FILE" | sed -e 's/_in/_out/');
      echo -e "${BLUE}Test: $IN_FILE${NC}";
      echo -e "${ORANGE}Reference output${REF_FILE}${NC}";
      $PROGRAM < "$IN_FILE" > /tmp/test/my_out.txt;

    done
  else
  echo -e "${BLUE} ------ Running the program with no input ------ ${NC}\n";
  $PROGRAM;
fi
# if program ended with code 0
if [ $? -eq 0 ]; then
  echo -e "${GREEN}TEST RUN: OK${NC}\n";
else
  echo -e "${RED}TEST RUN: Fail${NC}\n";
fi