FROM debian:stable-slim
LABEL authors="krystofprasil"
# https://github.com/somedevv/docker-valgrind original author

RUN apt-get update
RUN apt-get -yq upgrade
RUN apt-get -yq install sudo build-essential cmake valgrind libcppunit-dev
RUN rm -rf /var/lib/apt/lists/*


COPY ./prog-tests/. /prog-tests
RUN chmod -R 775 /prog-tests

ENTRYPOINT ["tail", "-f", "/dev/null"]